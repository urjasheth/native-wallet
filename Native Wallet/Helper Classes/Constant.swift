//
//  Constant.swift
//  Bevvi
//
//  Created by Hetal Govani on 01/12/17.
//  Copyright © 2017 solulab. All rights reserved.
//

import UIKit
let APP_NAME = "Native Wallet"

let GOOGLE_API_KEY = "AIzaSyAdWj2ykMjpJyIiJVOm4o85mNqaIQw5RHY"//client's acc
let OrangeColor = UIColor.init(red: 255/255, green: 136/255, blue: 99/255, alpha: 1)

let GradientColor1 = UIColor.init(red: 41/255, green: 166/255, blue: 211/255, alpha: 1)
let GradientColor2 = UIColor.init(red: 20/255, green: 188/255, blue: 248/255, alpha: 1)

let kNAVIGATIONCOLOR = UIColor.init(red: 41/255, green: 166/255, blue: 211/255, alpha: 1)
let kBARBUTTONCOLOR = UIColor(red: 1, green: 1, blue: 1, alpha: 1)
let appDel = UIApplication.shared.delegate as! AppDelegate


extension UIView {
    func layerGradient() {
        let layer : CAGradientLayer = CAGradientLayer()
        layer.frame.size = self.frame.size
        layer.frame.origin = CGPoint.zero
        
        let color1 = GradientColor1.cgColor
        let color2 = GradientColor2.cgColor
        
        layer.colors = [color1,color2]
        self.layer.insertSublayer(layer, at: 0)
    }
}
