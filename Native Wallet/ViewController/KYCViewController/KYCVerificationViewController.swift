//
//  KYCVerificationViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 27/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class KYCVerificationViewController: BaseViewController,KPDropMenuDelegate,TermsandConditionsDelegate
{
   
    
    @IBOutlet var viewNotVerified : UIView!
    @IBOutlet var viewVerified : UIView!
    @IBOutlet var dropdownCountry : KPDropMenu!
    @IBOutlet var dropdownState : KPDropMenu!
    @IBOutlet var dropdownCity : KPDropMenu!
    @IBOutlet var viewUserDetails : UIView!
    @IBOutlet var viewPhotoID : UIView!
    @IBOutlet var viewAgreement: UIView!
    @IBOutlet var btnContinueUserDetail : UIButton!
    @IBOutlet var btnContinuePhotoId : UIButton!
    @IBOutlet var btnBack : UIButton!
    @IBOutlet var btnBackAgreement : UIButton!
    @IBOutlet var btnUserDetail : UIButton!
    @IBOutlet var btnPhotoId : UIButton!
    @IBOutlet var btnAgreement : UIButton!
    var isCountrySelected : Bool! = false
    var isStateSelected : Bool! = false
    var isCitySelected : Bool! = false

    @IBOutlet var txtFirstName : customTextField!
    @IBOutlet var txtLastName : customTextField!
    @IBOutlet var txtDOB : customTextField!
    @IBOutlet var txtEmail : customTextField!
    @IBOutlet var txtPhone : customTextField!
    @IBOutlet var txtEtheriumAddress : customTextField!

    @IBOutlet var imgCountry : UIImageView!
    @IBOutlet var imgState : UIImageView!
    @IBOutlet var imgCity : UIImageView!

    @IBOutlet var lblError : UILabel!

    @IBOutlet var imgPhotoId : UIImageView!
    @IBOutlet var imgPhotoIdWithPerson : UIImageView!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "KYC Verification"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.layerGradient()

        dropdownCountry.delegate = self
        dropdownCountry.items = ["United States", "India", "Australia", "United Kingdom"];
        dropdownCountry.itemsFont = UIFont.init(name: "OpenSans", size: 15)
        dropdownCountry.titleTextAlignment = .left
        dropdownCountry.directionDown = true
        
        dropdownState.delegate = self
        dropdownState.items = ["Gujarat", "Rajasthan", "Mumbai", "New Delhi"];
        dropdownState.itemsFont = UIFont.init(name: "OpenSans", size: 15)
        dropdownState.titleTextAlignment = .left
        dropdownState.directionDown = true
        
        dropdownCity.delegate = self
        dropdownCity.items = ["Ahmedabad", "Baroda", "Jamnagar"];
        dropdownCity.itemsFont = UIFont.init(name: "OpenSans", size: 15)
        dropdownCity.titleTextAlignment = .left
        dropdownCity.directionDown = true
        
        viewPhotoID.isHidden = true
        viewUserDetails.isHidden = false
        viewAgreement.isHidden = true

        btnBack.layer.borderColor = GradientColor1.cgColor
        btnBack.layer.borderWidth = 0.5
        btnBackAgreement.layer.borderColor = GradientColor1.cgColor
        btnBackAgreement.layer.borderWidth = 0.5
        
        lblError.isHidden = true
        
        let datePickerView = UIDatePicker()
        datePickerView.datePickerMode = .date
        txtDOB.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(handleDatePicker(sender:)), for: .valueChanged)
    }
    @objc func handleDatePicker(sender: UIDatePicker)
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/yyyy"
        txtDOB.text = dateFormatter.string(from: sender.date)
    }
    @IBAction func btnTCPress(sender:UIButton)
    {
        let TCVC = TermsandConditionsViewController(nibName: "TermsandConditionsViewController", bundle: nil)
        
        TCVC.view.frame = CGRect.init(x: 18, y: TCVC.view.frame.origin.y+44, width: UIScreen.main.bounds.width - 36, height: TCVC.view.frame.height-88)
        TCVC.view.layer.cornerRadius = 5
        TCVC.strTitle = "Terms and Condition"
        TCVC.delegate=self

        self.presentPopupViewController(TCVC, animationType: MJPopupViewAnimationFade)
    }
    func DismissButtonClicked(_ secondDetailViewController: TermsandConditionsViewController)
    {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    @IBAction func btnPrivacyPolicyPress(sender:UIButton)
    {
        let TCVC = TermsandConditionsViewController(nibName: "TermsandConditionsViewController", bundle: nil)
        
        TCVC.view.frame = CGRect.init(x: 18, y: TCVC.view.frame.origin.y+44, width: UIScreen.main.bounds.width - 36, height: TCVC.view.frame.height-88)
        TCVC.view.layer.cornerRadius = 5
        TCVC.strTitle = "Privacy Policy"
        TCVC.delegate=self
        self.presentPopupViewController(TCVC, animationType: MJPopupViewAnimationFade)
    }
    func didSelectItem(_ dropMenu: KPDropMenu!, at atIndex: Int32)
    {
        if dropMenu == dropdownCountry
        {
            imgCountry.image = #imageLiteral(resourceName: "arrow_down")
            isCountrySelected = true
        }
        if dropMenu == dropdownState
        {
            imgState.image = #imageLiteral(resourceName: "arrow_down")
            isStateSelected = true
        }
        else if dropMenu == dropdownCity
        {
            imgCity.image = #imageLiteral(resourceName: "arrow_down")
            isCitySelected = true
        }
        print(atIndex)
    }
    @IBAction func btnContinueUserDetailPress(sender:UIButton)
    {
//        if validationUserDetail()
//        {
            btnUserDetail.setImage(#imageLiteral(resourceName: "user_approved"), for: .normal)
            btnPhotoId.setImage(#imageLiteral(resourceName: "id_active"), for: .normal)
            btnAgreement.setImage(#imageLiteral(resourceName: "tick_unactive"), for: .normal)
            viewUserDetails.isHidden = true
            viewPhotoID.isHidden = false
            viewAgreement.isHidden = true
//        }
    }
    func validationUserDetail() -> Bool
    {
        if isCountrySelected
        {
            imgCountry.image = #imageLiteral(resourceName: "arrow_down")
            lblError.isHidden = true
            if isStateSelected
            {
                imgState.image = #imageLiteral(resourceName: "arrow_down")
                lblError.isHidden = true
                if isCitySelected
                {
                    imgCity.image = #imageLiteral(resourceName: "arrow_down")
                    lblError.isHidden = true
                    if txtFirstName.text?.count != 0
                    {
                        txtFirstName.hideError()
                        lblError.isHidden = true
                        if txtLastName.text?.count != 0
                        {
                            txtLastName.hideError()
                            lblError.isHidden = true
                            if txtDOB.text?.count != 0
                            {
                                txtDOB.hideError()
                                lblError.isHidden = true
                                if txtEmail.text?.count != 0
                                {
                                    if UtilityClass.emailvalidate(txtEmail.text)
                                    {
                                        txtEmail.hideError()
                                        lblError.isHidden = true
                                        if txtPhone.text?.count != 0
                                        {
                                            txtPhone.hideError()
                                            lblError.isHidden = true
                                            if txtEtheriumAddress.text?.count != 0
                                            {
                                                txtEtheriumAddress.hideError()
                                                lblError.isHidden = true
                                                return true
                                            }
                                            else
                                            {
                                                txtEtheriumAddress.showError()
                                                lblError.isHidden = false
                                                lblError.text = "Please enter etherium address."
                                                return false
                                            }
                                        }
                                        else
                                        {
                                            txtPhone.showError()
                                            lblError.isHidden = false
                                            lblError.text = "Please enter phonr number."
                                            return false
                                        }
                                    }
                                    else
                                    {
                                        txtEmail.showError()
                                        lblError.isHidden = false
                                        lblError.text = "Please enter valid email address."
                                        return false
                                    }
                                }
                                else
                                {
                                    txtEmail.showError()
                                    lblError.isHidden = false
                                    lblError.text = "Please enter email address."
                                    return false
                                }
                            }
                            else
                            {
                                txtDOB.showError()
                                lblError.isHidden = false
                                lblError.text = "Please select date of birth."
                                return false
                            }
                        }
                        else
                        {
                            txtLastName.showError()
                            lblError.isHidden = false
                            lblError.text = "Please enter last name."
                            return false
                        }
                    }
                    else
                    {
                        txtFirstName.showError()
                        lblError.isHidden = false
                        lblError.text = "Please enter first name."
                        return false
                    }
                }
                else
                {
                    imgCity.image = #imageLiteral(resourceName: "error")
                    lblError.isHidden = false
                    lblError.text = "Please select city."
                    return false
                }
            }
            else
            {
                imgState.image = #imageLiteral(resourceName: "error")
                lblError.isHidden = false
                lblError.text = "Please select state."
                return false
            }
        }
        else
        {
            imgCountry.image = #imageLiteral(resourceName: "error")
            lblError.isHidden = false
            lblError.text = "Please select country."
            return false
        }
    }
//    func validationPhotoId() -> Bool {
    
//    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        btnUserDetail.setImage(#imageLiteral(resourceName: "user_active"), for: .normal)
        btnPhotoId.setImage(#imageLiteral(resourceName: "id_unactive"), for: .normal)
        btnAgreement.setImage(#imageLiteral(resourceName: "tick_unactive"), for: .normal)
        viewUserDetails.isHidden = false
        viewPhotoID.isHidden = true
        viewAgreement.isHidden = true
    }
    @IBAction func btnContinuePhotoIdPress(sender:UIButton)
    {
        btnUserDetail.setImage(#imageLiteral(resourceName: "user_approved"), for: .normal)
        btnPhotoId.setImage(#imageLiteral(resourceName: "id_approved"), for: .normal)
        btnAgreement.setImage(#imageLiteral(resourceName: "tick_active"), for: .normal)
        viewUserDetails.isHidden = true
        viewPhotoID.isHidden = true
        viewAgreement.isHidden = false
    }
    @IBAction func btnBackAgreementPress(sender:UIButton)
    {
        btnUserDetail.setImage(#imageLiteral(resourceName: "user_approved"), for: .normal)
        btnPhotoId.setImage(#imageLiteral(resourceName: "id_active"), for: .normal)
        btnAgreement.setImage(#imageLiteral(resourceName: "tick_unactive"), for: .normal)
        viewUserDetails.isHidden = true
        viewPhotoID.isHidden = false
        viewAgreement.isHidden = true
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
