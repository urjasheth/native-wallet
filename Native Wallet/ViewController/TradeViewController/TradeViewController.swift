//
//  TradeViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 25/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class TradeViewController: BaseViewController,UITableViewDataSource,UITableViewDelegate
{
    @IBOutlet var tblOrder : UITableView!
    @IBOutlet var tblObj : UITableView!
    var isTrade : Bool! = true
    var isMyOrder : Bool! = false
    var isHistory : Bool! = false
    
    @IBOutlet var cellBalance : UITableViewCell!
    @IBOutlet var cellTrade : UITableViewCell!
    @IBOutlet var cellMyOrder : UITableViewCell!
    @IBOutlet var cellHistory : UITableViewCell!

    @IBOutlet var lblTrade : UILabel!
    @IBOutlet var lblTradeLine : UILabel!
    @IBOutlet var lblMyOrder : UILabel!
    @IBOutlet var lblMyOrderLine : UILabel!
    @IBOutlet var lblHistory : UILabel!
    @IBOutlet var lblHistoryLine : UILabel!

    @IBOutlet var btnBuy : UIButton!
    @IBOutlet var btnSell : UIButton!
    
    @IBOutlet var btnBuyOrder : UIButton!
    @IBOutlet var btnSellOrder : UIButton!
    
    @IBOutlet var btnPlaceOrder : UIButton!

    @IBOutlet var viewBalance : UIView!
    @IBOutlet var viewPrice : UIView!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Trade"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.layerGradient()
//        let imageView = UIImageView(image: #imageLiteral(resourceName: "bg"))
//        self.tblObj.backgroundView = imageView
        tblObj.tableFooterView = UIView()
        
        selectTrade()
        tblObj.reloadData()
        viewPrice.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor
        viewBalance.layer.borderColor = UIColor.black.withAlphaComponent(0.5).cgColor

        viewPrice.layer.borderWidth = 0.5
        viewBalance.layer.borderWidth = 0.5
    }

    @IBAction func btnTradePress(sender:UIButton)
    {
        selectTrade()
        tblObj.reloadData()
    }
    @IBAction func btnBuyTradePress(sender:UIButton)
    {
        if isTrade
        {
            btnBuy.backgroundColor = GradientColor1
            btnSell.backgroundColor = UIColor.white
            
            btnBuy.setTitleColor(UIColor.white, for: .normal)
            btnSell.setTitleColor(OrangeColor, for: .normal)
            
            btnBuy.setImage(#imageLiteral(resourceName: "receive_coin"), for: .normal)
            btnSell.setImage(#imageLiteral(resourceName: "Send_coin_orange"), for: .normal)
            
            btnPlaceOrder.setTitle("Place an Order", for: .normal)
        }
        else
        {
            btnBuyOrder.backgroundColor = GradientColor1
            btnSellOrder.backgroundColor = UIColor.white
            
            btnBuyOrder.setTitleColor(UIColor.white, for: .normal)
            btnSellOrder.setTitleColor(OrangeColor, for: .normal)
            
            btnBuyOrder.setImage(#imageLiteral(resourceName: "receive_coin"), for: .normal)
            btnSellOrder.setImage(#imageLiteral(resourceName: "Send_coin_orange"), for: .normal)
        }
    }
    @IBAction func btnSellTradePress(sender:UIButton)
    {
        if isTrade
        {
            btnSell.backgroundColor = OrangeColor
            btnBuy.backgroundColor = UIColor.white
            
            btnSell.setTitleColor(UIColor.white, for: .normal)
            btnBuy.setTitleColor(GradientColor1, for: .normal)
            
            btnBuy.setImage(#imageLiteral(resourceName: "receive_coin_blue"), for: .normal)
            btnSell.setImage(#imageLiteral(resourceName: "send_coin"), for: .normal)
            
            btnPlaceOrder.setTitle("Sell", for: .normal)
        }
        else
        {
            btnSellOrder.backgroundColor = OrangeColor
            btnBuyOrder.backgroundColor = UIColor.white
            
            btnSellOrder.setTitleColor(UIColor.white, for: .normal)
            btnBuyOrder.setTitleColor(GradientColor1, for: .normal)
            
            btnBuyOrder.setImage(#imageLiteral(resourceName: "receive_coin_blue"), for: .normal)
            btnSellOrder.setImage(#imageLiteral(resourceName: "send_coin"), for: .normal)
        }
    }
    @IBAction func btnMyOrderPress(sender:UIButton)
    {
        selectMyOrder()
        tblObj.reloadData()
    }
    
    @IBAction func btnHistoryPress(sender:UIButton)
    {
        selectHistory()
        tblObj.reloadData()
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if tableView == tblObj
        {
            if indexPath.row == 0
            {
                return 360
            }
            else
            {
                if isTrade
                {
                    return 280
                }
                else if isMyOrder
                {
                    return 400
                }
                else
                {
                    return 460
                }
            }
        }
        else
        {
            return 50
        }
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        if tableView == tblObj
        {
            if isTrade
            {
                return 2
            }
            else if isMyOrder
            {
                return 2
            }
            else
            {
                return 2
            }
        }
        else
        {
            if isMyOrder
            {
                return 6
            }
            else
            {
                return 8
            }
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if tableView == tblObj
        {
            if indexPath.row == 0
            {
                cellBalance.selectionStyle = .none
                return cellBalance
            }
            else
            {
                if isTrade
                {
                    cellTrade.selectionStyle = .none
                    return cellTrade
                }
                else if isMyOrder
                {
                    cellMyOrder.selectionStyle = .none
                    tblOrder.reloadData()
                    return cellMyOrder
                }
                else
                {
                    cellHistory.selectionStyle = .none
                    tblOrder.reloadData()
                    return cellHistory
                }
            }
        }
        else
        {
            let cell = tblOrder.dequeueReusableCell(withIdentifier: "cell") as! tblOrderCell
            cell.selectionStyle = .none
            if indexPath.row == 0
            {
                cell.lblDate.text = "Date & Time"
                cell.lblQuantity.text = "Quantity"
                cell.lblRate.text = "Rate ($)"
                cell.lblAmount.text = "Amount ($)"
                
                cell.lblDate.textColor = GradientColor1
                cell.lblQuantity.textColor = GradientColor1
                cell.lblRate.textColor = GradientColor1
                cell.lblAmount.textColor = GradientColor1
            }
            else
            {
                cell.lblDate.text = "11/5/18 12:24 PM"
                cell.lblQuantity.text = "0.50000"
                cell.lblRate.text = "2250.00"
                cell.lblAmount.text = "1125.00"
                
                cell.lblDate.font = UIFont.init(name: "OpenSans", size: 12)
                cell.lblQuantity.font = UIFont.init(name: "OpenSans", size: 12)
                cell.lblRate.font = UIFont.init(name: "OpenSans", size: 12)
                cell.lblAmount.font = UIFont.init(name: "OpenSans", size: 12)
                if isMyOrder
                {
                    cell.lblDate.textColor = UIColor.black
                    cell.lblQuantity.textColor = UIColor.black
                    cell.lblRate.textColor = UIColor.black
                    cell.lblAmount.textColor = UIColor.black
                }
                else
                {
                    cell.lblDate.textColor = UIColor.black
                    cell.lblQuantity.textColor = UIColor.black
                    if indexPath.row == 3
                    {
                        cell.lblRate.textColor = OrangeColor
                    }
                    else if indexPath.row == 4
                    {
                        cell.lblRate.textColor = OrangeColor
                    }
                    else
                    {
                        cell.lblRate.textColor = GradientColor1
                    }
                    cell.lblAmount.textColor = UIColor.black
                }
                
            }
            return cell
        }
    }
    
    func selectTrade()
    {
        isTrade = true
        isHistory = false
        isMyOrder = false
        lblTrade.textColor = GradientColor1
        lblTradeLine.backgroundColor = GradientColor1
        lblMyOrder.textColor = GradientColor1.withAlphaComponent(0.7)
        lblMyOrderLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblHistory.textColor = GradientColor1.withAlphaComponent(0.7)
        lblHistoryLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        
        btnBuy.backgroundColor = GradientColor1
        btnSell.backgroundColor = UIColor.white
        btnBuy.setTitleColor(UIColor.white, for: .normal)
        btnSell.setTitleColor(OrangeColor, for: .normal)
        btnBuy.setImage(#imageLiteral(resourceName: "receive_coin"), for: .normal)
        btnSell.setImage(#imageLiteral(resourceName: "Send_coin_orange"), for: .normal)

        btnSell.tintColor = OrangeColor
        btnSell.layer.borderColor = OrangeColor.cgColor
        btnSell.layer.borderWidth = 0.5
        btnBuy.layer.borderColor = GradientColor1.cgColor
        btnBuy.layer.borderWidth = 0.5
    }
    func selectMyOrder()
    {
        isTrade = false
        isHistory = false
        isMyOrder = true
        lblTrade.textColor = GradientColor1.withAlphaComponent(0.7)
        lblTradeLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblMyOrder.textColor = GradientColor1
        lblMyOrderLine.backgroundColor = GradientColor1
        lblHistory.textColor = GradientColor1.withAlphaComponent(0.7)
        lblHistoryLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        
        btnBuyOrder.backgroundColor = GradientColor1
        btnSellOrder.backgroundColor = UIColor.white
        btnBuyOrder.setTitleColor(UIColor.white, for: .normal)
        btnSellOrder.setTitleColor(OrangeColor, for: .normal)
        btnBuyOrder.setImage(#imageLiteral(resourceName: "receive_coin"), for: .normal)
        btnSellOrder.setImage(#imageLiteral(resourceName: "Send_coin_orange"), for: .normal)
        
        btnSellOrder.tintColor = OrangeColor
        btnSellOrder.layer.borderColor = OrangeColor.cgColor
        btnSellOrder.layer.borderWidth = 0.5
        btnBuyOrder.layer.borderColor = GradientColor1.cgColor
        btnBuyOrder.layer.borderWidth = 0.5
    }
    func selectHistory()
    {
        isTrade = false
        isHistory = true
        isMyOrder = false
        lblTrade.textColor = GradientColor1.withAlphaComponent(0.7)
        lblTradeLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblMyOrder.textColor = GradientColor1.withAlphaComponent(0.7)
        lblMyOrderLine.backgroundColor = GradientColor1.withAlphaComponent(0.7)
        lblHistory.textColor = GradientColor1
        lblHistoryLine.backgroundColor = GradientColor1
    }
    
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }
}
class tblOrderCell : UITableViewCell
{
    @IBOutlet var lblQuantity : UILabel!
    @IBOutlet var lblRate : UILabel!
    @IBOutlet var lblAmount : UILabel!
    @IBOutlet var lblDate : UILabel!
}
