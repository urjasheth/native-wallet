//
//  NotificationViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 03/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class NotificationViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    override func viewDidLoad()
    {
        super.viewDidLoad()

        self.view.layerGradient()
        
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Notifications"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        let button1: UIButton = UIButton.init(type:UIButtonType.custom)
        button1.setImage(#imageLiteral(resourceName: "delete"), for: UIControlState.normal)
        button1.setTitle("  Clear All", for: .normal)
        button1.titleLabel?.font = UIFont.init(name: "OpenSans-Semibold", size: 12)
        button1.addTarget(self, action: #selector(self.btnClearAll), for: UIControlEvents.touchUpInside)
        button1.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton1 = UIBarButtonItem(customView: button1)
        self.navigationItem.rightBarButtonItem = barButton1
       
        tblObj.tableFooterView = UIView()
    }
    @objc func btnClearAll() {
        
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! notificationCell
        cell.selectionStyle = .none
        return cell
    }

    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
class notificationCell: UITableViewCell {
    
}
