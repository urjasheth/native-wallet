//
//  SignUpViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 22/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class SignUpViewController: UIViewController,SignupSuccessDelegate
{
    @IBOutlet var containerView : UIView!
    @IBOutlet var txtName : customTextField!
    @IBOutlet var txtEmail : customTextField!
    @IBOutlet var txtPhone : customTextField!
    @IBOutlet var txtReferralCode : customTextField!
    @IBOutlet var txtPassword : customTextField!
    @IBOutlet var txtConfirmPassword : customTextField!
    @IBOutlet var btnCheck : UIButton!
    @IBOutlet var lblError : UILabel!
    
    override var prefersStatusBarHidden: Bool
    {
        return true
    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = UIColor.init(patternImage: #imageLiteral(resourceName: "bg"))
        
        containerView.layer.cornerRadius = 6.7
        lblError.isHidden = true

    }
    @IBAction func btnBackPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnSignupPress(sender:UIButton)
    {
        if validate()
        {
            let signupSuccessVC = SignupSuccessViewController(nibName: "SignupSuccessViewController", bundle: nil)
            
            signupSuccessVC.view.frame = CGRect.init(x: 18, y: signupSuccessVC.view.frame.origin.y, width: UIScreen.main.bounds.width - 36, height: signupSuccessVC.view.frame.height)
            signupSuccessVC.view.layer.cornerRadius = 5
            
            signupSuccessVC.delegate=self
            self.presentPopupViewController(signupSuccessVC, animationType: MJPopupViewAnimationFade)
        }
    }
    @IBAction func btnCheckPress(sender:UIButton)
    {
        if !btnCheck.isSelected
        {
            btnCheck.setImage(#imageLiteral(resourceName: "checkbox_fill"), for: .normal)
            btnCheck.isSelected = true
        }
        else
        {
            btnCheck.setImage(#imageLiteral(resourceName: "checkbox_empty"), for: .normal)
            btnCheck.isSelected = false
        }
    }
    func validate() -> Bool
    {
        if txtName.text?.count != 0
        {
            txtName.hideError()
            lblError.isHidden = true
            if txtEmail.text?.count != 0
            {
                txtEmail.hideError()
                lblError.isHidden = true
                if UtilityClass.emailvalidate(txtEmail.text)
                {
                    txtEmail.hideError()
                    lblError.isHidden = true
                   if txtPhone.text?.count != 0
                   {
                    txtPhone.hideError()
                    lblError.isHidden = true
                    if txtPassword.text?.count != 0
                    {
                        txtPassword.hideError()
                        lblError.isHidden = true
                        if txtConfirmPassword.text?.count != 0
                        {
                            txtConfirmPassword.hideError()
                            lblError.isHidden = true
                            if txtPassword.text == txtConfirmPassword.text
                            {
                                txtConfirmPassword.hideError()
                                txtPassword.hideError()
                                lblError.isHidden = true
                                return true
                            }
                            else
                            {
                                txtConfirmPassword.showError()
                                txtPassword.showError()

                                lblError.text = ("Password and confirm password is not same.")
                                lblError.isHidden = false
//                                UtilityClass.showAlert("Password and confirm password is not same.")
                                return false
                            }
                        }
                        else
                        {
                            txtConfirmPassword.showError()
                            lblError.text = ("Please enter confirm password.")
                            lblError.isHidden = false
//                            UtilityClass.showAlert("Please enter confirm password.")
                            return false
                        }
                    }
                    else
                    {
                        txtPassword.showError()
                        lblError.text = "Please enter password."
                        lblError.isHidden = false
//                        UtilityClass.showAlert("Please enter password.")
                        return false
                    }
                   }
                   else
                   {
                        txtPhone.showError()
                        lblError.text = "Please enter phone number."
                        lblError.isHidden = false
//                        UtilityClass.showAlert("Please enter valid email.")
                        return false
                    }
                }
                else
                {
                    txtEmail.showError()
                    lblError.text = "Please enter valid email address."
                    lblError.isHidden = false
//                    UtilityClass.showAlert("Please enter valid email.")
                    return false
                }
            }
            else
            {
                txtEmail.showError()
                lblError.text = "Please enter email address."
                lblError.isHidden = false
//                UtilityClass.showAlert("Please enter email.")
                return false
            }
        }
        else
        {
            txtName.showError()
            lblError.text = "Please enter name."
            lblError.isHidden = false
//            UtilityClass.showAlert("Please enter name.")
            return false
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
