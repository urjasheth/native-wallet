//
//  ChangePasswordViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 04/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit
protocol ChangePasswordDelegate
{
    func SendButtonClicked(_ secondDetailViewController: ChangePasswordViewController)
}
class ChangePasswordViewController: UIViewController
{
    var delegate: ChangePasswordDelegate?
    @IBOutlet var lblError : UILabel!

    @IBOutlet var txtOldPassword : customTextField!
    @IBOutlet var txtNewPassword : customTextField!
    @IBOutlet var txtConfirmNewPassword : customTextField!

    override func viewDidLoad()
    {
        super.viewDidLoad()
        lblError.isHidden = true
    }

    @IBAction func btnDonePress(sender:UIButton)
    {
        if validate()
        {
            delegate?.SendButtonClicked(self)
        }
        
    }
    
    func validate() -> Bool {
        if txtOldPassword.text?.count != 0
        {
            txtOldPassword.hideError()
            lblError.isHidden = true
            if txtNewPassword.text?.count != 0
            {
                txtNewPassword.hideError()
                lblError.isHidden = true
                if txtConfirmNewPassword.text?.count != 0
                {
                    txtConfirmNewPassword.hideError()
                    lblError.isHidden = true
                    if txtNewPassword.text == txtConfirmNewPassword.text
                    {
                        txtConfirmNewPassword.hideError()
                        lblError.isHidden = true
                        return true
                    }
                    else
                    {
                        txtConfirmNewPassword.showError()
                        lblError.text = "New password and confirm new password not matching."
                        lblError.isHidden = false
                        return false
                    }
                }
                else
                {
                    txtConfirmNewPassword.showError()
                    lblError.text = "Please enter confirm new password."
                    lblError.isHidden = false
                    return false
                }
            }
            else
            {
                txtNewPassword.showError()
                lblError.text = "Please enter new password."
                lblError.isHidden = false
                return false
            }
        }
        else
        {
            txtOldPassword.showError()
            lblError.text = "Please enter old password."
            lblError.isHidden = false
            return false
        }
    }
    override func didReceiveMemoryWarning()
    {
        super.didReceiveMemoryWarning()
    }

}
