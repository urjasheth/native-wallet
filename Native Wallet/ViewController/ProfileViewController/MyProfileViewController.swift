//
//  MyProfileViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 03/07/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class MyProfileViewController: BaseViewController,ChangePasswordDelegate
{
    @IBOutlet var btnProfile : UIButton!
    @IBOutlet var imgProfile : UIImageView!
    @IBOutlet var viewHeight : NSLayoutConstraint!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.view.layerGradient()
        viewHeight.constant = self.view.bounds.height - 84
        self.navigationController?.navigationBar.isHidden = false
        self.title = "My Profile"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
    }
    func SendButtonClicked(_ secondDetailViewController: ChangePasswordViewController)
    {
        self.dismissPopupViewControllerWithanimationType(MJPopupViewAnimationFade)
    }
    @IBAction func btnChangePassword(sender:UIButton)
    {
        let ChangePasswordVC = ChangePasswordViewController(nibName: "ChangePasswordViewController", bundle: nil)
        
        ChangePasswordVC.view.frame = CGRect.init(x: 18, y: ChangePasswordVC.view.frame.origin.y, width: UIScreen.main.bounds.width - 36, height: ChangePasswordVC.view.frame.height)
        ChangePasswordVC.view.layer.cornerRadius = 5
        
        ChangePasswordVC.delegate=self
        self.presentPopupViewController(ChangePasswordVC, animationType: MJPopupViewAnimationFade)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
