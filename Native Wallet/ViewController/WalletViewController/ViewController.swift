//
//  ViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 21/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class ViewController: BaseViewController,UITableViewDelegate,UITableViewDataSource
{
    @IBOutlet var tblObj : UITableView!
    @IBOutlet var cell1 : UITableViewCell!
    @IBOutlet var cell2 : UITableViewCell!

    override var prefersStatusBarHidden: Bool
    {
        return false
    }
//    override var preferredStatusBarStyle: UIStatusBarStyle
//    {
//        return .lightContent
//    }
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.title = "Wallet"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "menu"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(BaseViewController.onSlideMenuButtonPressed(_:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.layerGradient()
//        let imageView = UIImageView(image: #imageLiteral(resourceName: "bg"))
//        self.tblObj.backgroundView = imageView
//        tblObj.tableFooterView = UIView()
    }
   
    @IBAction func btnSendPress(sender:UIButton)
    {
        let sendCoinObj = self.storyboard?.instantiateViewController(withIdentifier: "SendCoinsViewController") as! SendCoinsViewController
        self.navigationController?.pushViewController(sendCoinObj, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return 2
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.row == 0
        {
            if UIScreen.main.bounds.height == 568
            {
                return 400
            }
            return 370
        }
        else
        {
            if UIScreen.main.bounds.height == 568
            {
                return 530
            }
            return 500
        }
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if indexPath.row == 0
        {
            cell1.selectionStyle = .none
            return cell1
        }
        else
        {
            cell2.selectionStyle = .none
            return cell2
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

