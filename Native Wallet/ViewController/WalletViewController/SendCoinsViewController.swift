//
//  SendCoinsViewController.swift
//  Native Wallet
//
//  Created by Solulab_Mosam on 25/06/18.
//  Copyright © 2018 Solulab_Mosam. All rights reserved.
//

import UIKit

class SendCoinsViewController: UIViewController
{
    @IBOutlet var txtamount : customTextField!
    @IBOutlet var txtAddress : customTextField!
    @IBOutlet var imgCoin : UIImageView!
    
    @IBOutlet var lblError : UILabel!
    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = false
        self.title = "Send Coin"
        
        let button: UIButton = UIButton.init(type:UIButtonType.custom)
        button.setImage(#imageLiteral(resourceName: "back"), for: UIControlState.normal)
        button.addTarget(self, action: #selector(self.backButtonPress(sender:)), for: UIControlEvents.touchUpInside)
        button.frame = CGRect.init(x: 0, y: 0, width: 30, height: 30)
        
        let barButton = UIBarButtonItem(customView: button)
        self.navigationItem.leftBarButtonItem = barButton
        
        self.view.layerGradient()
        lblError.isHidden = true
    }
    @IBAction func backSendPress(sender:UIButton)
    {
        if validation()
        {
            
        }
    }
    func validation() -> Bool
    {
        if txtamount.text?.count != 0
        {
            imgCoin.image = #imageLiteral(resourceName: "coin")
            lblError.isHidden = true
            if txtAddress.text?.count != 0
            {
                txtAddress.hideError()
                lblError.isHidden = true
                return true
            }
            else
            {
                txtAddress.showError()
                lblError.isHidden = true
                lblError.text = "please enter address."
                return false
            }
        }
        else
        {
            imgCoin.image = #imageLiteral(resourceName: "error")
            lblError.isHidden = false
            lblError.text = "please enter amount to send."
            return false
        }
    }
    @IBAction func backButtonPress(sender:UIButton)
    {
        self.navigationController?.popViewController(animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}
